package models;

import org.slf4j.LoggerFactory;

import javax.persistence.Entity;

@Entity
public class AddressChangeTask extends UserTask {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(AddressChangeTask.class);

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
