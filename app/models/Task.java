package models;

import org.slf4j.LoggerFactory;

import javax.persistence.*;

@Entity
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public class Task {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(Task.class);

    @Id
    @GeneratedValue
    private long id;

    long userId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
