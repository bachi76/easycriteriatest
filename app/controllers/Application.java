package controllers;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import models.AddressChangeTask;
import models.Task;
import models.UserTask;
import play.*;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.*;

import views.html.*;

import java.util.List;

public class Application extends Controller {

    @Transactional
    public static Result index() {

        UserTask ut1 = new UserTask();
        ut1.setUserId(1);
        JPA.em().persist(ut1);

        UserTask ut2 = new UserTask();
        ut2.setUserId(2);
        JPA.em().persist(ut2);

        AddressChangeTask act = new AddressChangeTask();
        act.setUserId(2);
        JPA.em().persist(act);

        EasyCriteria<Task> query = EasyCriteriaFactory.createQueryCriteria(JPA.em(), Task.class);
        query.andEquals("userId", 2);

        long count = query.count();

        List<Task> result = query.getResultList();
        long countResultList = result.size();

        return ok("Both counts should match:\n" +
                ".count() result: " + count + "\n" +
                "Result objects returned by .getResultList(): " + countResultList);
    }
}
