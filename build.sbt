name := "EasyCriteriaTest"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaJpa,
  cache,
  "org.eclipse.persistence" % "eclipselink" % "2.5.2", // Note: needed only here since for now javaJpa is not supporting JPA 2.1 yet
  "uaihebert.com" % "EasyCriteria" % "3.0.0"
)

playJavaSettings ++ Seq(
  ebeanEnabled := false
)
